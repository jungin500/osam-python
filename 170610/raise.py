def rsp(mine, yours):
	allowed = ['가위', '바위', '보']
	if mine not in allowed or yours not in allowed :
	    raise ValueError("{}와 {}는 잘못된 값입니다.".format(mine, yours))

try:
	rsp('가위', '바위')
	print('가위, 바위 성공')
	rsp('보', '바')
	print('''엥? '보'랑 '바'가 성공.''') 
except ValueError as ex:
	print(''''보'랑 '바'에서 ValueError 발생! - {}'''.format(ex))
