class Calculator:
	def mul(x, y):
		return x * y

	@staticmethod
	def add(x, y):
		return x + y

	@classmethod
	def sub(x, y):
		return x - y

print(Calculator.mul(10, 20))
print(Calculator.add(10, 20))
print(Calculator.sub(10, 20))
